# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo houses the bootstrap scripts and heat templates used by the United States Army Cyber School.  Everything in this repository is licensed under the GNU GPLv3.

There is no stable branch yet, so everything could randomly change from day-to-day without warning.  This repo will constantly be updated as we write more templates in house.