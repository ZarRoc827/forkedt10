heat_template_version: 2015-10-15

description: Generic OpenStack Deployment Script

parameters:

  base_string:
    type: string
    label: Base String
    description: The unique string which will form the base of all initial resource names in your deployed environment
    default: changeme
    constraints:
      -  length: { min: 3, max: 10 }
         description: Base name must be between 3 and 10 characters
      -  allowed_pattern: "[a-z]*"
         description: Base name must contain only lower case letters

  num_centos6:
    type: number
    label: Choose the number of CentOS 6 VMs you wish to spawn
    description: This will determine the number of CentOS 6 VMs you will spawn.  Hostnames will be in the form [Base Hostname]-centos6-[num], where num is an iterator starting at 1.
    default: 1
    constraints:
      - range: { min: 0, max: 10 }

  num_centos7:
    type: number
    label: Choose the number of CentoS 7 VMs you wish to spawn
    description: This will determine the number of CentOS 7 VMs you will spawn.  Hostnames will be in the form [Base Hostname]-centos7-[num], where num is an iterator starting at 1.
    default: 1
    constraints:
      - range: { min: 0, max: 10 }

  num_ubuntu14:
    type: number
    label: Choose the number of Ubuntu 14.04 VMs you wish to spawn
    description: This will determine the number of Ubuntu 14.04 VMs you will spawn.  Hostnames will be in the form [Base Hostname]-ubuntu14-[num], where num is an iterator starting at 1.
    default: 1
    constraints:
      - range: { min: 0, max: 10 }

  num_ubuntu16:
    type: number
    label: Choose the number of Ubuntu 16.04 VMs you wish to spawn
    description: This will determine the number of Ubuntu 16.04 VMs you will spawn.  Hostnames will be in the form [Base Hostname]-ubuntu16-[num], where num is an iterator starting at 1.
    default: 1
    constraints:
      - range: { min: 0, max: 10 }

  num_suse:
    type: number
    label: Choose the number of SUSE Leap 42.1 VMs you wish to spawn
    description: This will determine the number of SUSE Leap 42.1 VMs you will spawn.  Hostnames will be in the form [Base Hostname]-suse-[num], where num is an iterator starting at 1.
    default: 1
    constraints:
      - range: { min: 0, max: 10 }

  num_fedora:
    type: number
    label: Choose the number of Fedora 24 VMs you wish to spawn
    description: This will determine the number of Fedora 24 VMs you will spawn.  Hostnames will be in the form [Base Hostname]-fedora-[num], where num is an iterator starting at 1.
    default: 1
    constraints:
      - range: { min: 0, max: 10 }

  allow_tcp_ports:
    type: comma_delimited_list
    label: Allowed inbound TCP ports
    description: Provide a CSV list of TCP ports that you would like to be allowed on the inbound via your security groups.
    default: "22"

  allow_udp_ports:
    type: comma_delimited_list
    label: Allowed inbound UDP ports
    description: Provide a CSV list of UDP ports that you would like to be allowed on the inbound via your security groups.
    default: "162"

  allow_ICMP:
    type: boolean
    label: Allow ICMP?
    description: Choose whether or not to allow ICMP on the inbound via security groups.
    default: False

resources:

  network:
    type: https://bitbucket.org/bitskrieg/openstack/raw/master/heat/network/private.yaml
    properties:
      base_string: { get_param: base_string }

  debian:
    type: https://bitbucket.org/bitskrieg/openstack/raw/master/heat/os/debian.yaml
    depends_on: network
    properties:
      base_string: { get_param: base_string }
      network: { get_attr: [network, name] }

  generic-sec-group-tcp:
    type: OS::Neutron::SecurityGroup
    properties:
      description: Enable User-Specified inbound TCP ports via Security Groups
      name: Ports_TCP_Enable
      rules:
        repeat:
          for_each:
            <%port%>: { get_param: allow_tcp_ports }
          template:
            protocol: tcp
            port_range_min: <%port%>
            port_range_max: <%port%>

  generic-sec-group-udp:
    type: OS::Neutron::SecurityGroup
    properties:
      description: Enable User-Specified inbound UDP ports via Security Groups
      name: Ports_UDP_Enable
      rules:
        repeat:
          for_each:
            <%port%>: { get_param: allow_udp_ports }
          template:
            protocol: udp
            port_range_min: <%port%>
            port_range_max: <%port%>

  generic-sec-group-icmp:
    type: OS::Neutron::SecurityGroup
    properties:
      description: Enable inbound ICMP via Security Groups
      name: ICMP_Enable
      rules:
        - protocol: icmp



