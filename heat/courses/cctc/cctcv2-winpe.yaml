heat_template_version: 2015-10-15

description: CCTC Student Lane Heat Template Version 2
#added salt
#Upgraded Win7 to SP1
#Removed kali repos (left katoolin so they could be re-enabled)
#Ran yum update & apt-get update/upgrade
#Windows TS Practice VM

parameters:

  student_id:
    type: string
    label: Student ID
    description: Student ID Number
    default: 0

  first_name:
    type: string
    label: First Name
    description: First Name
    default: 
    constraints:
      -  length: { min: 1, max: 15 }
         description: First name must be no longer than 15 characters
      -  allowed_pattern: "[a-zA-Z]*"
         description: First name may only contain letters

  last_name:
    type: string
    label: Last Name
    description: Last Name
    default: 
    constraints:
      -  length: { min: 1, max: 15 }
         description: Last name must be no longer than 15 characters
      -  allowed_pattern: "[a-zA-Z]*"
         description: Last name may only contain letters
  
  root_password:
    type: string
    label: Root Password
    description: root password for *nix VMs
    hidden: true
    default: changeme
    constraints:
      -  length: { min: 8, max: 20 }
         description: Password must be between 8 and 20 characters
      -  allowed_pattern: "[a-zA-Z0-9]*"
         description: Password may not contain special characters

  safe_mode_administrator_password:
    label: Domain Administrator Password
    default: changeme
    description: Domain Administrator Password
    hidden: true
    type: string
    constraints:
      - length: { min: 8, max: 64 }
        description: Password must be at least 8 characters
      - allowed_pattern: "[a-zA-Z0-9]*"
        description: Password may not contain special characters

resources:

  stu-network:
    type: OS::Neutron::Net
    properties:
      name:
        str_replace:
          template: lastName_Network
          params:
            lastName: { get_param: last_name }

  stu-subnet:
    type: OS::Neutron::Subnet
    properties:
      allocation_pools:
        - start:
            str_replace:
              template: 10.studentID.0.5
              params:
                studentID: { get_param: student_id }
          end:
            str_replace:
              template: 10.studentID.0.250
              params:
                studentID: { get_param: student_id }
      cidr:
        str_replace:
          template: 10.studentID.0.0/24
          params:
            studentID: { get_param: student_id }
      gateway_ip:
        str_replace:
          template: 10.studentID.0.254
          params:
            studentID: { get_param: student_id }
      network: { get_resource: stu-network }
      dns_nameservers:
        str_split: 
          - ','
          - str_replace:
              template: 10.studentID.0.1,172.16.0.254
              params:
                studentID: { get_param: student_id }            
      name:
        str_replace:
          template: lastname_subnet
          params:
            lastname: { get_param: last_name }

  stu-router:
    type: OS::Neutron::Router    
    properties:
      name:
        str_replace:
          template: lastname_router
          params:
            lastname: { get_param: last_name }
      external_gateway_info: {"network": Floating Network}

  stu-router-interface:
    type:  OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: stu-router }       
      subnet_id: { get_resource: stu-subnet }

  stu-float-ip:
    type: OS::Neutron::FloatingIP
    depends_on: stu-router
    properties:
      floating_network: Floating Network

  stu-float-port:
    type: OS::Neutron::Port
    depends_on: stu-sec-group
    properties:
      network_id: { get_resource: stu-network }
      fixed_ips:
      - subnet_id: { get_resource: stu-subnet }
        ip_address:
          str_replace:
            template: 10.studentID.0.2
            params:
              studentID: { get_param: student_id }        
      security_groups:
      - { get_resource: stu-sec-group }

  stu-float-ip-assoc:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: {get_resource: stu-float-ip}
      port_id: {get_resource: stu-float-port}

  stu-sec-group:
    type: OS::Neutron::SecurityGroup
    properties:
      description: Enable RDP and ICMP for the jump box
      name: RDP/ICMP Enable
      rules: [{"port_range_max": 3389,"port_range_min": 3389, "protocol":TCP},{"protocol": ICMP}]

  stu-sec-group-dc:
    type: OS::Neutron::SecurityGroup
    properties:
      description: Enable DNS for the DC
      name: DNS Enable
      rules: [{"port_range_max": 65535,"port_range_min": 0, "protocol":TCP},{"port_range_max": 65535,"port_range_min": 0, "protocol":UDP},{"protocol":ICMP}]

  server0:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-Lubuntu
          params:
            lastname: { get_param: last_name }
      image: Lubuntu 14.04
      flavor: CCTC.small
      networks: 
        - network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            echo "root:$password" | chpasswd
            sed -i 's/localhost/localhost lubuntuHostname/g' /etc/hosts
            echo "deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/latest trusty main">>/etc/apt/sources.list
            wget -O - https://repo.saltstack.com/apt/ubuntu/14.04/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -
            apt-get update
            apt-get install salt-minion -y
            reboot
          params:
            $password: { get_param: root_password }
            lubuntuHostname: 
              str_replace:
                template: lastname-Lubuntu
                params: 
                  lastname: { get_param: last_name }      
      user_data_format: RAW       

  server1:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-kali
          params:
            lastname: { get_param: last_name }
      image: Debian 8 Jessie (Katoolin)
      flavor: CCTC.medium
      networks: 
        - network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            echo "root:$password" | chpasswd
            sed -i 's/localhost/localhost kaliHostname/g' /etc/hosts
            echo "deb http://debian.saltstack.com/debian jessie-saltstack main">>/etc/apt/sources.list
            wget -q -O- "http://debian.saltstack.com/debian-salt-team-joehealy.gpg.key" | apt-key add -
            apt-get update
            apt-get install salt-minion -y
            reboot
          params:
            $password: { get_param: root_password }
            kaliHostname: 
              str_replace:
                template: lastname-kali
                params: 
                  lastname: { get_param: last_name }      
      user_data_format: RAW
      
  server2:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-centos
          params:
            lastname: { get_param: last_name }
      image: CentOS 7
      flavor: CCTC.medium
      networks: 
        - network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            echo "root:$password" | chpasswd
            sed -i 's/localhost.*/localhost centosHostname/g' /etc/hosts
            echo centosHostname>/etc/hostname
            yum install https://repo.saltstack.com/yum/redhat/salt-repo-2015.8-2.el7.noarch.rpm -y
            yum clean expire-cache
            yum install salt-minion -y
            systemctl enable salt-minion.service
            systemctl start salt-minion.service  
            reboot
          params:
            $password: { get_param: root_password }
            centosHostname: 
              str_replace:
                template: lastname-centos
                params: 
                  lastname: { get_param: last_name }       
      user_data_format: RAW

  server3:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-xpsp3
          params:
            lastname: { get_param: last_name }
      image: Windows XP SP3
      flavor: CCTC.small
      networks: 
        - network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }

  server4:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-xpsp2
          params:
            lastname: { get_param: last_name }
      image: Windows XP SP2
      flavor: CCTC.small
      networks: 
        - network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }

  server5:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-ms2
          params:
            lastname: { get_param: last_name }
      image: Metasploitable 2
      flavor: CCTC.medium
      networks: 
        - network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }      

  server6:
    type: OS::Nova::Server
    properties:
      image: Windows Server 2012 R2
      flavor: CCTC.xlarge
      name:
        str_replace:
          template: lastname-dc
          params:
            lastname: { get_param: last_name }
      networks: 
        - fixed_ip:
            str_replace:
              template: 10.studentID.0.1
              params:
                studentID: { get_param: student_id }
          network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }
      user_data:
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'Stop'
            secedit /export /cfg c:\secpol.cfg
            (gc C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg
            secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg /areas SECURITYPOLICY
            rm -force c:\secpol.cfg -confirm:$false
            netsh advfirewall set allprofiles state off
            Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
            $user = [ADSI]'WinNT://./Administrator'
            # Disable user
            #$user.userflags = 2
            #$user.SetInfo()
            $user.SetPassword('safe_mode_administrator_password')
            Import-Module ADDSDeployment
            $safeModePwd = (ConvertTo-SecureString 'safe_mode_administrator_password' -AsPlainText -Force)
            Install-ADDSForest -DomainName 'domain_name' -DomainNetbiosName 'domain_netbios_name' -SafeModeAdministratorPassword $safeModePwd -InstallDns -NoRebootOnCompletion -Force
            Add-DnsServerForwarder 172.16.0.254
            Invoke-WebRequest http://web.cctc.mil/dist/salt-x64.exe -Outfile $env:temp\salt.exe
            .$env:temp\salt.exe /S /master=salt.cctc.mil /minion-name=dcHostname
            exit 1001
          params:
            safe_mode_administrator_password: { get_param: safe_mode_administrator_password }
            domain_name:
              str_replace:
                template: lastname.cctc.mil
                params:
                  lastname: { get_param: last_name }
            domain_netbios_name:
              str_replace:
                template: lastname
                params:
                  lastname: { get_param: last_name }
            dcHostname:
              str_replace:
                template: lastname-dc
                params:
                  lastname: { get_param: last_name }

  server7:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-win7
          params:
            lastname: { get_param: last_name }
      image: Windows 7
      flavor: CCTC.large
      networks: 
        - port: {get_resource: stu-float-port}
      user_data:
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'Stop'
            netsh advfirewall set allprofiles state off
            set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server'-name "fDenyTSConnections" -Value 0
            netsh interface ip set address name="Local Area Connection" source=static ip_address 255.255.255.0 dGateway
            netsh interface ipv4 set dns name="Local Area Connection" source=static address=DNSserver primary
            netsh interface ipv4 add dnsserver name="Local Area Connection" address=172.16.0.254 index=2
            Start-Sleep -s 900
            Invoke-WebRequest http://web.cctc.mil/dist/salt-x86.exe -Outfile $env:temp\salt.exe
            .$env:temp\salt.exe /S /master=salt.cctc.mil /minion-name=w7Hostname
            $domain = "domain_name"
            $password = "admin_password" | ConvertTo-SecureString -asPlainText -Force
            $username = "$domain\administrator" 
            $credential = New-Object System.Management.Automation.PSCredential($username,$password)
            Add-Computer -DomainName $domain -Credential $credential
            exit 1001
          params:
            domain_name:
              str_replace:
                template: lastname.cctc.mil
                params:
                  lastname: { get_param: last_name }
            admin_password: { get_param: safe_mode_administrator_password }
            ip_address:
              str_replace:
                template: 10.studentID.0.2
                params:
                  studentID: { get_param: student_id }
            dGateway:
              str_replace:
                template: 10.studentID.0.254
                params:
                  studentID: { get_param: student_id }
            DNSserver:
              str_replace:
                template: 10.studentID.0.1
                params:
                  studentID: { get_param: student_id }
            w7Hostname:
              str_replace:
                template: lastname-win7
                params:
                  lastname: { get_param: last_name }

  server8:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-winTS
          params:
            lastname: { get_param: last_name }
      image: Windows 7-TS
      flavor: CCTC.large
      networks: 
        - fixed_ip:
            str_replace:
              template: 10.studentID.0.3
              params:
                studentID: { get_param: student_id }
          network: { get_resource: stu-network }
      security_groups: 
        - { get_resource: stu-sec-group-dc }
      user_data:
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'Stop'
            netsh interface ip set address name="Local Area Connection" source=static ip_address 255.255.255.0 dGateway
            netsh interface ipv4 set dns name="Local Area Connection" source=static address=DNSserver primary
            netsh interface ipv4 add dnsserver name="Local Area Connection" address=172.16.0.254 index=2
            exit 1001
          params:
            ip_address:
              str_replace:
                template: 10.studentID.0.3
                params:
                  studentID: { get_param: student_id }
            dGateway:
              str_replace:
                template: 10.studentID.0.254
                params:
                  studentID: { get_param: student_id }
            DNSserver:
              str_replace:
                template: 10.studentID.0.1
                params:
                  studentID: { get_param: student_id }
            w7Hostname:
              str_replace:
                template: lastname-winTS
                params:
                  lastname: { get_param: last_name }

outputs:
  floating_ip:
    description: Windows 7 Jump Box floating IP for RDP Access
    value: { get_attr : [stu-float-ip, floating_ip_address] }
