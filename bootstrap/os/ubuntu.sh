#!/bin/bash

#Ubuntu Bootstrap Script
#This script will update the local repositories, install curl (if needed), and pull
#the latest bootstrap script for saltstack directly from saltstack

apt-get update
apt-get -y install curl
curl -L -o /tmp/bootstrap.sh https://bootstrap.saltstack.com 
chmod +x /tmp/bootstrap.sh
#For additional command line options see:
#https://docs.saltstack.com/en/latest/topics/tutorials/salt_bootstrap.html
/tmp/bootstrap.sh stable
rm /tmp/bootstrap.sh
reboot
